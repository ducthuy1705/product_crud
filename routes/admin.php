<?php

use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ProductCategoriesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Models\ProductCategories;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*------ Auth ------*/
Auth::routes();

/*------ Admin ------*/
Route::prefix('admin')->middleware('auth')->name('admin.')->group( function(){

    /*------ User ------*/
    Route::prefix('user')->name('user.')->middleware('hasRole:admin,visitor')->group( function(){
        $class = UserController::class;
        Route::get('/', [$class, 'index'])->name('index')->middleware('hasPermission:user.view');
        Route::post('store', [$class, 'store'])->name('store')->middleware('hasPermission:user.create');
        Route::get('edit/{id}', [$class, 'edit'])->name('edit')->middleware('hasPermission:user.update');
        Route::post('update', [$class, 'update'])->name('update')->middleware('hasPermission:user.update');
        Route::delete('delete/{id}', [$class, 'delete'])->name('delete')->middleware('hasPermission:user.delete');
    });

    /*------ Role ------*/
    Route::prefix('role')->name('role.')->middleware('hasRole:admin,visitor')->group( function(){
        $class = RoleController::class;
        Route::get('/', [$class, 'index'])->name('index')->middleware('hasPermission:role.view');
        Route::post('store', [$class, 'store'])->name('store')->middleware('hasPermission:role.view');
        Route::get('edit/{id}', [$class, 'edit'])->name('edit')->middleware('hasPermission:role.view');
        Route::post('update', [$class, 'update'])->name('update')->middleware('hasPermission:role.view');
        Route::delete('delete/{id}', [$class, 'delete'])->name('delete')->middleware('hasPermission:role.view');
    });

    /*------ Permission ------*/
    Route::prefix('permission')->name('permission.')->middleware('hasRole:admin,visitor')->group( function(){
        $class = PermissionsController::class;
        Route::get('/', [$class, 'index'])->name('index')->middleware('hasPermission:permission.view');
        Route::post('store', [$class, 'store'])->name('store')->middleware('hasPermission:permission.create');
        Route::post('update', [$class, 'update'])->name('update')->middleware('hasPermission:permission.update');
        Route::delete('delete/{id}', [$class, 'delete'])->name('delete')->middleware('hasPermission:permission.delete');
    });
    
    /*------ Product Category ------*/
    Route::prefix('product-categories')->name('product-categories.')->middleware('hasRole:admin,visitor')->group( function(){
        $class = ProductCategoriesController::class;
        Route::get('/', [$class, 'index'])->name('index')->middleware('hasPermission:productCategories.view');
        Route::post('store', [$class, 'store'])->name('store')->middleware('hasPermission:productCategories.create');
        Route::post('update', [$class, 'update'])->name('update')->middleware('hasPermission:productCategories.update');
        Route::delete('delete/{id}', [$class, 'delete'])->name('delete')->middleware('hasPermission:productCategories.delete');
    });
    
    /*------ Product ------*/
    Route::prefix('product')->name('product.')->middleware('hasRole:admin,visitor')->group( function(){
        $class = ProductsController::class;
        Route::get('/', [$class, 'index'])->name('index')->middleware('hasPermission:product.view');
        Route::post('store', [$class, 'store'])->name('store')->middleware('hasPermission:product.create');
        Route::get('edit/{id}', [$class, 'edit'])->name('edit')->middleware('hasPermission:product.update');
        Route::post('update', [$class, 'update'])->name('update')->middleware('hasPermission:product.update');
        Route::delete('delete/{id}', [$class, 'delete'])->name('delete')->middleware('hasPermission:product.delete');
    });
});