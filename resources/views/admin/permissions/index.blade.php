@extends('admin.layouts.main')

@section('content')
    <div class="page-content">
        <h4 class="text-uppercase">Permissions</h4>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="d-flex align-items-center justify-content-between">
                        @can('permission.create')
                            <div class="col-lg-4 col-xl-4">
                                <a href="" class="btn btn-primary mb-3 mb-lg-0" data-bs-toggle="modal"
                                    data-bs-target="#create-permission">
                                    <i class="bx bxs-plus-square"></i>Thêm mới
                                </a>
                            </div>
                        @endcan
                        @include('admin.components.search', ['route' => route("admin.permission.index")])
                    </div>
                </div>
                @include("admin.components.notification")
                @include("admin.permissions.list")
            </div>
            {{ $permissions->links() }}
        </div>

        {{-- Modal for create product --}}
        @include('admin.permissions.create')
        {{-- Modal for update product --}}
        @include('admin.permissions.update')
    </div>
@endsection
