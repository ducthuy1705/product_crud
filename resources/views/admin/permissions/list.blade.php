<table class="table mt-4">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
            <th scope="col">Created At</th>
            @can(['permission.delete', 'permission.update'])
                <th scope="col">Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @foreach ($permissions as $permission)
            <tr>
                <td>{{ $permission->id }}</td>
                <td>{{ $permission->name }}</td>
                <td>{{ $permission->action }}</td>
                <td>{{ date_format($permission->created_at, 'd/m/Y H:i:s') }}</td>
                <td>
                    @can(['permission.update'])
                        <a class="btn btn-success update-buttuon" 
                            data-url="{{ route("admin.permission.update", ['id' => $permission->id]) }}"
                            data-bs-toggle="modal" data-bs-target="#update-permission">
                            <i class="bx bx-edit mr-0"></i>
                        </a>
                    @endcan

                    @can(['permission.delete'])
                        <a class="btn btn-danger delete-button"
                            data-url="{{ route('admin.permission.delete', ['id' => $permission->id]) }}">
                            <i class="bx bx-trash mr-0"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
