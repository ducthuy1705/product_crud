<div class="modal fade" id="update-permission" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cập nhật permission</h5>
            </div>
            <form id="formUpdatePermission" method="POST" data-url="{{ route('admin.permission.update') }}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id-permission">
                    <div class="form-group mb-3">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control" id="name-permission">
                    </div>

                    <div class="form-group mb-3">
                        <label for="">Action</label>
                        <input type="text" name="action" class="form-control" id="action-permission">
                        <span class="text-danger d-none" id="actionUpdateError"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary submit-update">Gửi</a>
                </div>
            </form>
        </div>
    </div>
</div>
