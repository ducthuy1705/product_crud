<div class="sweet-alert">
    <div class="alert alert-success border-0 bg-success alert-dismissible fade show py-2">
        <div class="d-flex align-items-center">
            <div class="font-35 text-white"><i class="bx bxs-check-circle"></i>
            </div>
            <div class="ms-3">
                <h6 class="mb-0 text-white">Success Alerts</h6>
                <div class="text-white" id="message"></div>
            </div>
        </div>
    </div>
</div>