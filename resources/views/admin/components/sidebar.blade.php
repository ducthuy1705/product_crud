<div class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{ asset('admin/assets/images/logo-icon.png') }}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h4 class="logo-text">Deha</h4>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu">
        <li class="menu-label">Resources</li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-cart-alt'></i>
                </div>
                <div class="menu-title">Sản phẩm</div>
            </a>
            <ul>
                <li> <a href="{{ route('admin.product.index') }}"><i class="bx bx-right-arrow-alt"></i>Danh sách sản phẩm</a>
                </li>
                <li> <a href="{{ route('admin.product-categories.index') }}"><i class="bx bx-right-arrow-alt"></i>Danh sách danh mục</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-user'></i>
                </div>
                <div class="menu-title">User</div>
            </a>
            <ul>
                <li> <a href="{{ route('admin.user.index') }}"><i class="bx bx-right-arrow-alt"></i>Danh sách user</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-lock'></i>
                </div>
                <div class="menu-title">Role</div>
            </a>
            <ul>
                <li> <a href="{{ route('admin.role.index') }}"><i class="bx bx-right-arrow-alt"></i>Danh sách role</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-lock'></i>
                </div>
                <div class="menu-title">Permissions</div>
            </a>
            <ul>
                <li> <a href="{{ route('admin.permission.index') }}"><i class="bx bx-right-arrow-alt"></i>Danh sách permissions</a>
                </li>
            </ul>
        </li>

    </ul>
    <!--end navigation-->
</div>
