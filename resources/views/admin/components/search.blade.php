<div class="position-relative">
    <form action="{{ $route }}" method="GET">
        <input type="text" class="form-control ps-5" placeholder="Search Anything..." name="search_key" value="{{ request('search_key') }}"> 
        <span class="position-absolute top-50 product-show translate-middle-y">
            <i class="bx bx-search"></i>
        </span>
    </form>
</div>