<div class="modal fade" id="update-productCategories" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cập nhật role</h5>
            </div>
            <form id="formUpdateProductCategories" method="POST" data-url="{{ route('admin.product-categories.update') }}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id-productCategories">
                    <div class="form-group mb-3">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control" id="name-productCategories">
                        <span class="text-danger d-none" id="nameUpdateError"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Slug</label>
                        <input type="text" name="slug" class="form-control" id="slug-productCategories">
                    </div>
                    <div class="form-group mb-5">
                        <label>
                            <input type="hidden" name="status" value="inactive">
                            <input type="checkbox" name="status" value="active" id="active-productCategories">
                            Active
                        </label>
                    </div>
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary submit-update">Gửi</a>
                </div>
            </form>
        </div>
    </div>
</div>
