<!-- Modal -->
<div class="modal fade" id="create-product-categories" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm mới Product Category</h5>
            </div>
            <form data-url="{{ route('admin.product-categories.store') }}" id="formCreateProductCategories" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control input-name">
                        <span class="text-danger d-none" id="nameError"></span>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Slug</label>
                        <input type="text" name="slug" class="form-control input-slug">
                        <span class="text-danger d-none" id="nameError"></span>
                    </div>
                    <div class="form-group mb-5">
                        <label>
                            <input type="hidden" name="status" value="inactive">
                            <input type="checkbox" name="status" value="active">
                            Active
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </div>
            </form>
        </div>
    </div>
</div>
