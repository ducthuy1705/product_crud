<table class="table mt-4">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Slug</th>
            <th scope="col">Status</th>
            <th scope="col">Created At</th>
            @can(['productCategories.delete', 'productCategories.update'])
                <th scope="col">Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @foreach ($productCategories as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->slug }}</td>
                @if($item->status == "active")
                    <td><span class="badge bg-success">{{ $item->status }}</span></td>
                @else
                    <td><span class="badge bg-danger">{{ $item->status }}</span></td>
                @endif
                <td>{{ date_format($item->created_at, 'd/m/Y H:i:s') }}</td>
                <td>
                    @can(['productCategories.update'])
                        <a class="btn btn-success update-productC-button"
                            data-bs-toggle="modal" data-bs-target="#update-productCategories">
                            <i class="bx bx-edit mr-0"></i>
                        </a>
                    @endcan

                    @can(['productCategories.delete'])
                        <a class="btn btn-danger delete-button"
                            data-url="{{ route('admin.product-categories.delete', ['id' => $item->id]) }}">
                            <i class="bx bx-trash mr-0"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
