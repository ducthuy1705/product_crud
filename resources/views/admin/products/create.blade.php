<!-- Modal -->
<div class="modal fade" id="create-product" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm mới Product</h5>
            </div>
            <form data-url="{{ route('admin.product.store') }}" id="formCreateProduct" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6 mb-3">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control input-name">
                            <span class="text-danger d-none" id="nameError"></span>
                        </div>
                        <div class="form-group col-md-6 mb-3">
                            <label for="">Slug</label>
                            <input type="text" name="slug" class="form-control input-slug">
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Description</label>
                        <textarea name="description" id="" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 mb-3">
                            <label for="">Price Base</label>
                            <input type="text" name="price_base" class="form-control">
                        </div>
                        <div class="form-group col-md-6 mb-3">
                            <label for="">Price Final</label>
                            <input type="text" name="price_total" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 mb-3">   
                            <label for="">Quantity</label>
                            <input type="number" name="quantity" class="form-control">
                        </div>
                        <div class="form-group col-md-6 mb-3 d-flex flex-column">
                            <label for="">Category</label>
                            <select name="category_id" id="select-category">
                                @foreach ($categories as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Iamge</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="hidden" name="status" value="inactive">
                            <input type="checkbox" name="status" value="active">
                            Active
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </div>
            </form>
        </div>
    </div>
</div>
