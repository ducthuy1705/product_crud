@extends('admin.layouts.main')

@section('content')
    <div class="page-content">
        <h4 class="text-uppercase">Product</h4>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="d-flex align-items-center justify-content-between">
                        @can('product.create')
                            <div class="col-lg-4 col-xl-4">
                                <a href="" class="btn btn-primary mb-3 mb-lg-0" data-bs-toggle="modal"
                                    data-bs-target="#create-product">
                                    <i class="bx bxs-plus-square"></i>Thêm mới
                                </a>
                            </div>
                        @endcan
                        <div class="position-relative">
                            <form action="{{ route("admin.product.index") }}" method="GET" id="formSearch">
                                <div class="d-flex align-items-center">
                                    <div class="search-option">
                                        <select id="select-role" name="search_option">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->name }}" {{ request("search_option") == $category->name ? "selected":"" }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="search-input position-relative" style="margin:0 10px">
                                        <input type="text" class="form-control ps-5" placeholder="Search Anything..." name="search_key" value="{{ request('search_key') }}"> 
                                        <span class="position-absolute top-50 product-show translate-middle-y">
                                            <i class="bx bx-search"></i>
                                        </span>
                                    </div>
                                    <button type="submit" class="btn btn-success">Search</button>
                                    <a href="#" class="btn btn-secondary clear-search" style="margin-left: 10px">Clear</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @include("admin.products.list")
            </div>
            {{ $products->links() }}
        </div>

        {{-- Modal for create product --}}
        @include('admin.products.create')
        {{-- Modal for create product --}}
        @include('admin.products.update')
    </div>
@endsection
