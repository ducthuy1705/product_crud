<table class="table mt-4">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Image</th>
            <th scope="col">Price Base</th>
            <th scope="col">Price Final</th>
            <th scope="col">Category</th>
            <th scope="col">Quantity</th>
            <th scope="col">Status</th>
            <th scope="col">Created At</th>
            @can(['product.delete', 'product.update'])
                <th scope="col">Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->description }}</td>
                <td>
                    <img src="{{ $item->getImage() }}" alt="{{ $item->name }}" class="image-show">
                </td>
                <td>{{ $item->price_base }}</td>
                <td>{{ $item->price_total }}</td>
                @php
                    $category = $item->category;
                    $category_name = @$category->name;
                @endphp
                <td>{{ $category_name }}</td>
                <td>{{ $item->quantity }}</td>
                @if($item->status == "active")
                    <td><span class="badge bg-success">{{ $item->status }}</span></td>
                @else
                    <td><span class="badge bg-danger">{{ $item->status }}</span></td>
                @endif
                <td>{{ date_format($item->created_at, 'd/m/Y H:i:s') }}</td>
                <td>
                    @can(['product.update'])
                        <a class="btn btn-success update-product-button"
                            data-bs-toggle="modal" data-bs-target="#update-product" 
                            data-url="{{ route('admin.product.edit', ['id' => $item->id]) }}">
                            <i class="bx bx-edit mr-0"></i>
                        </a>
                    @endcan

                    @can(['product.delete'])
                        <a class="btn btn-danger delete-button"
                            data-url="{{ route('admin.product.delete', ['id' => $item->id]) }}">
                            <i class="bx bx-trash mr-0"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
