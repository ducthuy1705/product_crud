<table class="table mt-4">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Avatar</th>
            <th scope="col">Phone</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">Created At</th>
            @can(['user.delete', 'user.update'])
                <th scope="col">Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @if (count($users) <= 0)
            <tr>
                <td colspan="8" class="text-center">Không tìm thấy kết quả !!</td>
            </tr>
        @else
            @foreach ($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>
                        <img class="image-show" src="{{ $user->getImage() }}" alt="">
                    </td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach ($user->roles as $role)
                            <span class="badge bg-success mr-2">{{ $role->name }}</span>
                        @endforeach
                    </td>
                    <td>{{ date_format($user->created_at, 'd/m/Y H:i:s') }}</td>
                    <td>
                        @can(['user.update'])
                            <a class="btn btn-success update-button"
                                data-bs-toggle="modal" data-bs-target="#update-user"
                                data-url={{ route('admin.user.edit', ['id' => $user->id]) }}>
                                <i class="bx bx-edit mr-0"></i>
                            </a>
                        @endcan

                        @can(['user.delete'])
                            <a class="btn btn-danger delete-button"
                                data-url="{{ route('admin.user.delete', ['id' => $user->id]) }}">
                                <i class="bx bx-trash mr-0"></i>
                            </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
