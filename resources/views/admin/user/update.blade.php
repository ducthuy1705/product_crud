<!-- Modal -->
<div class="modal fade select2-update" id="update-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm mới User</h5>
            </div>
            <form data-url="{{ route('admin.user.update') }}" id="formUpdateUser" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="id-update"> 
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <div class="d-flex flex-column"> 
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control input-name" id="name-update">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label for="">Phone</label>
                            <input type="text" name="phone" class="form-control" id="phone-update">
                            <span class="text-danger d-none" id="phoneUpdateError"></span>
                        </div>
    
                        <div class="col-md-6">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" autocomplete="off" id="email-update">
                            <span class="text-danger d-none" id="emailUpdateError"></span>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" autocomplete="off">
                            <span class="text-danger d-none" id="passwordUpdateError"></span>
                        </div>
    
                        <div class="col-md-6">
                            <label for="">Re-password</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                    </div>

                    <div class="d-flex flex-column mb-4">
                        <label for="form-label">Roles</label>
                        <select class="multiple-select-update" name="roles[]" multiple="multiple" id="roles-update">
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ Str::ucfirst($role->name) }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger d-none" id="rolesUpdateError"></span>
                    </div>

                    <div class="form-group file-upload">
                        <input type="file" name="avatar" class="form-control">
                        <div class="preview-img mt-3">
                            <img class="image-show" src="" alt="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </div>
            </form>
        </div>
    </div>
</div>
