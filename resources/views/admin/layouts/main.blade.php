<!doctype html>
<html lang="en" class="semi-dark">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="crsf-token" content="{{ csrf_token() }}">
    @include('admin.layouts.style')
	<title>Deha - Crud Demo</title>
</head>

<body>
	<div class="wrapper">
		
		@include('admin.components.sidebar')
		
		@include('admin.components.alert')

		@include('admin.layouts.header')

		<div class="page-wrapper">
			@yield('content')
		</div>

		<div class="overlay toggle-icon"></div>

		<a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>

		@include('admin.layouts.footer')
	</div>
	
	@include('admin.components.config')

	@include('admin.layouts.script')
</body>

</html>