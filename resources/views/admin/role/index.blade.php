@extends('admin.layouts.main')

@section('content')
    <div class="page-content">
        <h4 class="text-uppercase">Role</h4>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="d-flex align-items-center justify-content-between">
                        @can('role.create')
                            <div class="col-lg-4 col-xl-4">
                                <a href="" class="btn btn-primary mb-3 mb-lg-0" data-bs-toggle="modal"
                                    data-bs-target="#create-role">
                                    <i class="bx bxs-plus-square"></i>Thêm mới
                                </a>
                            </div>
                        @endcan
                        @include('admin.components.search', ['route' => route("admin.role.index")])
                    </div>
                </div>
                @include("admin.components.notification")
                @include("admin.role.list")
            </div>
        </div>

        {{-- Modal for create product --}}
        @include('admin.role.create')
        {{-- Modal for create product --}}
        @include('admin.role.update')
    </div>
@endsection
