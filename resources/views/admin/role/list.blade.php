<table class="table mt-4">
    <thead class="table-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Status</th>
            <th scope="col">Created At</th>
            @can(['role.delete', 'role.update'])
                <th scope="col">Actions</th>
            @endcan
        </tr>
    </thead>
    <tbody>
        @foreach ($roles as $role)
            <tr>
                <th scope="row">{{ $role->id }}</th>
                <td>{{ $role->name }}</td>
                @if($role->status == "active")
                    <td><span class="badge bg-success">{{ $role->status }}</span></td>
                @else
                    <td><span class="badge bg-danger">{{ $role->status }}</span></td>
                @endif
                <td>{{ date_format($role->created_at, 'd/m/Y H:i:s') }}</td>
                <td>
                    @can(['role.update'])
                        <a class="btn btn-success get-role-button" 
                            data-bs-toggle="modal" data-bs-target="#update-role"
                            data-url="{{ route("admin.role.edit", ['id' => $role->id]) }}">
                            <i class="bx bx-edit mr-0"></i>
                        </a>
                    @endcan

                    @can(['role.delete'])
                        <a class="btn btn-danger delete-button"
                            data-url="{{ route('admin.role.delete', ['id' => $role->id]) }}">
                            <i class="bx bx-trash mr-0"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
