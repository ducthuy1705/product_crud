<div class="modal fade" id="update-role" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cập nhật role</h5>
            </div>
            <form id="formUpdateRole" method="POST" data-url="{{ route('admin.role.update') }}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id" id="id-role">
                    <div class="form-group mb-3">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control" id="name-role">
                        <span class="text-danger d-none" id="nameUpdateError"></span>
                    </div>
                    <div class="form-group mb-5">
                        <label>
                            <input type="hidden" name="status" value="inactive">
                            <input type="checkbox" name="status" value="active" id="active-role">
                            Active
                        </label>
                    </div>
                    <div class="row check-permission">
                        <div class="d-flex align-items-center">
                            <label style="font-weight: 500; margin-right: 15px">Permissions</label>
                            <label>
                                <input type="checkbox" class="checkAllBox">
                                Check All
                            </label>
                        </div>
                        <span class="text-danger d-none" id="permissionsUpdateError"></span>
                        @foreach ($permissions as $permission)
                            <div class="col-4">
                                <div class="mt-2">
                                    <div class="form-group">
                                        <label class="checkbox-wrap">
                                            <input type="checkbox" value="{{$permission->id}}" name="permissions[{{$permission->action}}]" class="check-all"> 
                                            {{ $permission->name }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary submit-update">Gửi</a>
                </div>
            </form>
        </div>
    </div>
</div>
