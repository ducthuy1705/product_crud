<?php

namespace Database\Seeders;

use App\Models\Permissions;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'admin',
            'status' => 'active',
        ]);

        $admin->permissions()->attach([1,2,3,4,5,6,7,8]);

        $visitor = Role::create([
            'name' => 'visitor',
            'status' => 'active',
        ]);

        $visitor->permissions()->attach([1]);
        
    }
}
