<?php

namespace Database\Seeders;

use App\Models\Permissions;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission1 = Permissions::create([
            'name' => 'List Permission',
            'action' => 'permission.view'
        ]);

        $permission2 = Permissions::create([
            'name' => 'Create Permission',
            'action' => 'permission.create'
        ]);

        $permission3 = Permissions::create([
            'name' => 'Update Permission',
            'action' => 'permission.update'
        ]);

        $permission4 = Permissions::create([
            'name' => 'Delete Permission',
            'action' => 'permission.delete'
        ]);

        $permission5 = Permissions::create([
            'name' => 'Delete user',
            'action' => 'user.view'
        ]);

        $permission6 = Permissions::create([
            'name' => 'Delete user',
            'action' => 'user.create'
        ]);

        $permission7 = Permissions::create([
            'name' => 'Delete user',
            'action' => 'user.update'
        ]);

        $permission8 = Permissions::create([
            'name' => 'Delete user',
            'action' => 'user.delete'
        ]);
    }
}
