<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name', 'admin')->first();
        $visitor = Role::where('name', 'visitor')->first();

        $userAdmin = User::create([
            'name' => 'Đức Thủy',
            'email' => 'ducthuy1705@gmail.com',
            'password' => bcrypt('12345678')
        ]);

        $userAdmin->roles()->attach($admin);

        $userVisitor = User::create([
            'name' => 'Nguyễn Quý',
            'email' => 'quy@gmail.com',
            'password' => bcrypt('12345678')
        ]);

        $userVisitor->roles()->attach($visitor);
    }
}
