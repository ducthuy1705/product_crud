<?php

namespace Database\Factories;

use App\Models\ProductCategories;
use App\Models\Products;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Products::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->paragraph(),
            'image' => $this->faker->sentence(),
            'price_base' => $this->faker->randomNumber(),
            'price_total' => $this->faker->randomNumber(),
            'quantity' => $this->faker->randomNumber(),
            'status' => $this->faker->randomNumber(),
            'slug' => $this->faker->name(),
        ];
    }
}
