<?php

namespace Tests\Feature\Auth\Permission;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;
use App\Models\ProductCategories;

class UpdateProductCategoriesTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.product-categories.index');
    }

    public function getAdminRole()
    {
        return Role::where('name', 'admin')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_update_product_category_button()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertDontSeeText('Actions');
    }

    /** @test*/
    public function user_is_admin_can_see_update_product_category_button()
    {
        $role = $this->getAdminRole();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertSeeText('Actions');
    }

    /** @test*/
    public function admin_can_update_record()
    {
        $role = $this->getAdminRole();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $productCategory = ProductCategories::factory()->create();
        $productCategory->name = "HELLO";

        $response = $this->post('/admin/product-categories/update', $productCategory->toArray());
        $response->assertStatus(200);
        $this->assertDatabaseHas('product_categories', ['id' => $productCategory->id, 'name' => "Hello"]);
    }
}
