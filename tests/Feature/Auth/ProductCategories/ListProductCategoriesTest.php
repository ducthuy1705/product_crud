<?php

namespace Tests\Feature\Auth\Permission;

use App\Models\User;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListProductCategoriesTest extends TestCase
{

    public function getIndexRoute()
    {
        return route('admin.product-categories.index');
    }

    /** @test*/
    public function any_user_can_see_product_categories_list()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertViewIs('admin.productCategories.index');
    }
}
