<?php

namespace Tests\Feature\Auth\Permission;

use App\Models\ProductCategories;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class CreateProductCategoriesTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.product-categories.index');
    }

    public function getCreateRoute()
    {
        return route('admin.product-categories.store');
    }

    public function getRoleAdmin()
    {
        return Role::where('name', 'admin')->first();
    }

    public function getRoleVisitor()
    {
        return Role::where('name', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_create_product_categories_button()
    {
        $role = $this->getRoleVisitor();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Thêm mới');
    }

    /** @test*/
    public function user_is_admin_can_see_create_product_categories_button()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Thêm mới');
    }

    /** @test*/
    public function a_product_categories_requires_a_action()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $permission = ProductCategories::factory()->make(['name' => null]);

        $response = $this->post($this->getCreateRoute(), $permission->toArray());
        $response->assertStatus(302);
    }

    /** @test*/
    public function admin_can_create_product_categories()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $productCa = ProductCategories::factory()->make()->toArray();

        $response = $this->post($this->getCreateRoute(), $productCa);
        $response->assertStatus(200);
        $this->assertDatabaseHas('product_categories', $productCa);
    }
}
