<?php

namespace Tests\Feature\Auth\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class DeleteUserTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.user.index');
    }

    /** @test*/
    public function user_isnt_admin_cant_see_delete_user_button()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(403);
        $response->assertDontSeeText('Actions');
    }

    /** @test*/
    public function user_is_admin_can_see_delete_user_button()
    {
        $role = Role::where('name', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertSeeText('Actions');
    }
}
