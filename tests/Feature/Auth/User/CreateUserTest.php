<?php

namespace Tests\Feature\Auth\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class CreateUserTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.user.index');
    }

    public function getCreateRoute()
    {
        return route('admin.user.store');
    }

    public function getRoleAdmin()
    {
        return Role::where('name', 'admin')->first();
    }

    public function getRoleVisitor()
    {
        return Role::where('name', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_create_user_button()
    {
        $role = $this->getRoleVisitor();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Role');
    }

    /** @test*/
    public function user_is_admin_can_see_create_user_button()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Thêm mới');
    }

}
