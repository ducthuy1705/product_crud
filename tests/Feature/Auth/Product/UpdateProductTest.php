<?php

namespace Tests\Feature\Auth\Product;

use App\Models\Role;
use App\Models\User;
use App\Models\Products;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{

    public function getUpdateRoute()
    {
        return route('admin.product.update');
    }

    /** @test*/
    public function admin_can_update_a_product()
    {

        $role = Role::where('name', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $product = Products::factory()->create();
        $product->name = "HELLO";

        $response = $this->post($this->getUpdateRoute(), $product->toArray());
        $response->assertStatus(200);
        $this->assertDatabaseHas('products', ['id' => $product->id, 'name' => 'HELLO']);
    }

    /** @test*/
    public function isnot_admin_cant_update_a_product()
    {

        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $product = Products::factory()->create();
        $product->name = "HELLO";

        $response = $this->post($this->getUpdateRoute(), $product->toArray());
        $response->assertStatus(403);
    }
}
