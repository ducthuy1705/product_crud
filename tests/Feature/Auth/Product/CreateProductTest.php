<?php

namespace Tests\Feature\Auth\Product;

use App\Models\Products;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.product.index');
    }

    public function getCreateRoute()
    {
        return route('admin.product.store');
    }

    public function getRoleAdmin()
    {
        return Role::where('name', 'admin')->first();
    }

    public function getRoleVisitor()
    {
        return Role::where('name', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_create_products_button()
    {
        $role = $this->getRoleVisitor();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Thêm mới');
    }

    /** @test*/
    public function user_is_admin_can_see_create_products_button()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Thêm mới');
    }

    /** @test*/
    public function admin_can_create_new_products()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $products = Products::factory()->make()->toArray();

        $response = $this->post($this->getCreateRoute(), $products);
        $response->assertStatus(200);
        $this->assertDatabaseHas('products', $products);
    }
}
