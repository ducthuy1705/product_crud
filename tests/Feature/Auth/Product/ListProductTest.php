<?php

namespace Tests\Feature\Auth\Product;

use App\Models\Products;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;

class ListProductTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.product.index');
    }

    /** @test*/
    public function any_user_can_see_product_list()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertViewIs('admin.products.index');
    }
}
