<?php

namespace Tests\Feature\Auth\Product;

use App\Models\Products;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{

    public function getDeleteRoute($id)
    {
        return route('admin.product.delete', $id);
    }
    /** @test*/
    public function admin_can_delete_a_product()
    {
        $role = Role::where('name', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $product = Products::factory()->create();
        
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(200);
        $this->assertDatabaseMissing('products', $product->toArray());
    }

    /** @test*/
    public function is_not_admin_cant_delete_a_product()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $product = Products::factory()->create();
        
        $response = $this->delete($this->getDeleteRoute($product->id));
        $response->assertStatus(403);
        $this->assertDatabaseHas('products', $product->toArray());
    }
}
