<?php

namespace Tests\Feature\Auth\Role;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class AuthorizeReadRoleTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.role.index');
    }

    /** @test*/
    public function unauthorized_user_cant_see_index_role_page()
    {
        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    /** @test*/
    public function authorized_user_can_see_index_role_page()
    {
        $role = Role::where('slug', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertViewIs('admin.role.index');
    }
}
