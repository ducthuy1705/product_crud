<?php

namespace Tests\Feature\Auth\Role;

use App\Models\Products;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class AuthorizeCreateRoleTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.role.index');
    }

    public function getCreateRoute()
    {
        return route('admin.role.store');
    }

    public function getRoleAdmin()
    {
        return Role::where('slug', 'admin')->first();
    }

    public function getRoleVisitor()
    {
        return Role::where('slug', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_create_button()
    {
        $role = $this->getRoleVisitor();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Create New Role');
    }

    /** @test*/
    public function user_is_admin_can_see_create_button()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Thêm mới');
    }
    
    /** @test*/
    // public function admin_can_store_a_role()
    // {
    //     $role = $this->getRoleAdmin();
    //     $user = User::factory()->create();
    //     $user->roles()->attach($role);
    //     $this->actingAs($user);

    //     $role = Role::factory()->make()->toArray();

    //     $response = $this->post($this->getCreateRoute(),$role);
    //     $response->assertStatus(201);
    //     $this->assertDatabaseHas('role', $role);
    // }
}
