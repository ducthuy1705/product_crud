<?php

namespace Tests\Feature\Auth\Role;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class AuthorizeUpdateRole extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.role.index');
    }

    public function getVisitorRole()
    {
        return Role::where('slug', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_update_button()
    {
        $role = $this->getVisitorRole();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertDontSeeText('Actions');
    }

    /** @test*/
    public function user_is_admin_can_see_update_button()
    {
        $role = Role::where('slug', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertSeeText('Actions');
    }

    /** @test*/
    public function user_isnt_admin_cant_get_edit_page()
    {
        $roleVisitor = $this->getVisitorRole();
        $user = User::factory()->create();
        $user->roles()->attach($roleVisitor);
        $this->actingAs($user);
        $role = Role::factory()->create();
        $id_role = $role->id;

        $response = $this->get('admin/role/edit/'.$id_role);
        $response->assertStatus(403);
    }

}
