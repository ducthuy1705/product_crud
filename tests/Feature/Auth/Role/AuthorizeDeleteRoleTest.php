<?php

namespace Tests\Feature\Auth\Role;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthorizeDeleteRoleTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.role.index');
    }

    /** @test*/
    public function user_isnt_admin_cant_see_delete_button()
    {
        $role = Role::where('slug', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertDontSeeText('Actions');
    }

    /** @test*/
    public function user_is_admin_can_see_delete_button()
    {
        $role = Role::where('slug', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertSeeText('Actions');
    }

    // /** @test*/
    // public function user_is_admin_can_delete_role()
    // {
    //     $role = Role::where('slug', 'admin')->first();
    //     $user = User::factory()->create();
    //     $user->roles()->attach($role);
    //     $this->actingAs($user);

    //     $role = Role::factory()->create();
    //     $role_id = $role->id;

    //     $this->delete('admin/role/delete/'.$role_id);
    //     $this->assertDatabaseMissing('roles', $role->toArray());
    // }
}
