<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Response;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function getProductIndexRoute()
    {
        return route('admin.permission.index');
    }

    /** @test*/
    public function unauthenticated_user_cant_get_index_page()
    {
        $response = $this->get($this->getProductIndexRoute());
        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

}
