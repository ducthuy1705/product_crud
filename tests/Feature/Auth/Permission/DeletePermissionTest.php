<?php

namespace Tests\Feature\Auth\Permission;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeletePermissionTest extends TestCase
{
    /** @test*/
    public function admin_can_delete_a_permission()
    {
        $role = Role::where('name', 'admin')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $permission = Permissions::factory()->create()->toArray();

        $response = $this->delete(route('admin.permission.delete', ['id' => $permission['id']]));
        $response->assertStatus(200);
        $this->assertDatabaseMissing('permissions', $permission);
    }

    /** @test*/
    public function is_not_admin_cant_delete_a_permission()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $permission = Permissions::factory()->create()->toArray();

        $response = $this->delete(route('admin.permission.delete', ['id' => $permission['id']]));
        $response->assertStatus(403);
    }
}
