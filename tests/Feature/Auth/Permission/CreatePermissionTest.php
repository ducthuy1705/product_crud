<?php

namespace Tests\Feature\Auth\Permission;

use App\Models\Permissions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\User;

class CreatePermissionTest extends TestCase
{
    public function getIndexRoute()
    {
        return route('admin.permission.index');
    }

    public function getCreateRoute()
    {
        return route('admin.permission.store');
    }

    public function getRoleAdmin()
    {
        return Role::where('name', 'admin')->first();
    }

    public function getRoleVisitor()
    {
        return Role::where('name', 'visitor')->first();
    }

    /** @test*/
    public function user_isnt_admin_cant_see_create_permission_button()
    {
        $role = $this->getRoleVisitor();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertDontSeeText('Thêm mới');
    }

    /** @test*/
    public function user_is_admin_can_see_create_permission_button()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertSeeText('Thêm mới');
    }

    /** @test*/
    public function admin_can_create_new_permission()
    {
        $role = $this->getRoleAdmin();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $permission = Permissions::factory()->make()->toArray();

        $response = $this->post($this->getCreateRoute(), $permission);
        $response->assertStatus(200);
        $this->assertDatabaseHas('permissions', $permission);
    }
}
