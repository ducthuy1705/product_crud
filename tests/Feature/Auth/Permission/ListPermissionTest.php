<?php

namespace Tests\Feature\Auth\Permission;

use App\Models\User;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListPermissionTest extends TestCase
{

    public function getIndexRoute()
    {
        return route('admin.permission.index');
    }

    /** @test*/
    public function any_user_can_see_permission_list()
    {
        $role = Role::where('name', 'visitor')->first();
        $user = User::factory()->create();
        $user->roles()->attach($role);
        $this->actingAs($user);

        $response = $this->get($this->getIndexRoute());
        $response->assertStatus(200);
        $response->assertViewIs('admin.permissions.index');
    }
}
