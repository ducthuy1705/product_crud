<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCategoriesRequest;
use App\Models\ProductCategories;
use App\Services\ProductCategoriesService;
use Illuminate\Http\Request;

class ProductCategoriesController extends Controller
{
    protected $productCategoriesService;

    public function __construct(ProductCategoriesService $productCategoriesService)
    {
        $this->productCategoriesService = $productCategoriesService;
    }

    public function index(Request $request)
    {
        $productCategories = $this->productCategoriesService->searchDataByKey($request->search_key);
        return view('admin.productCategories.index', compact('productCategories'));
    }

    public function store(ProductCategoriesRequest $request)
    {
        $this->productCategoriesService->saveData($request->all());
        return $this->sendResponse("Thêm mới thành công");
    }

    public function update(ProductCategoriesRequest $request)
    {
        $this->productCategoriesService->updateByID($request->all());
        return $this->sendResponse("Cập nhật thành công");
    }

    public function delete($id)
    {
        $this->productCategoriesService->deleteByID($id);
        return $this->sendResponse();
    }
}
