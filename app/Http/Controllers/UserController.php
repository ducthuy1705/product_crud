<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Http\Request;
use App\Services\UserService;

class UserController extends Controller
{ 
    protected $userService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->searchDataByKey($request);
        $roles = $this->roleService->getAll();
        return view("admin.user.index", compact('users', 'roles'));
    }

    public function store(UserRequest $request)
    {
        $this->userService->saveData($request);
        return $this->sendResponse("Thêm mới thành công");
    }


    public function edit($id)
    {
        $data = $this->userService->getUserByID($id);
        
        return response()->json([
            'roles' => $data['roles'],
            'user' => $data['user'],
            'status_code' => 200 
        ]);
    }

    public function update(UserUpdateRequest $request)
    {
        $this->userService->updateUser($request);
        return $this->sendResponse("Cập nhật thành công");
    }

    public function delete($id)
    {
        $this->userService->deleteByID($id);
        return $this->sendResponse();
    }
}
