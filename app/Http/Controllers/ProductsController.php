<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductsRequest;
use App\Models\Products;
use App\Services\ProductCategoriesService;
use App\Services\ProductsService;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    protected $productsService;
    protected $pathView = 'admin.products.';

    public function __construct(ProductsService $productsService, ProductCategoriesService $productCategoriesService)
    {
        $this->productsService = $productsService;
        $this->productCategoriesService = $productCategoriesService;
    }

    public function index(Request $request)
    {
        $products = $this->productsService->searchDataByKey($request);
        $categories = $this->productCategoriesService->getAll();
        return view($this->pathView.'index', compact('products', 'categories'));
    }

    public function store(ProductsRequest $request)
    {
        $this->productsService->saveData($request->all());
        return $this->sendResponse("Thêm mới thành công");
    }

    public function edit($id)
    {
        $product = $this->productsService->getDataByID($id);
        return response()->json([
            'status_code' => 200,
            'product' => $product,
        ]);
    }

    public function update(ProductsRequest $request)
    {
        $this->productsService->updateByID($request->all());
        return $this->sendResponse("Cập nhật thành công");
    }

    public function delete($id)
    {
        $this->productsService->deleteByID($id);
        return $this->sendResponse();
    }
}
