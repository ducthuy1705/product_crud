<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Services\PermissionsService;
use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{

    protected $roleService;
    protected $pathView = 'admin.role.';

    public function __construct(RoleService $roleService, PermissionsService $permissionsService)
    {
        $this->roleService = $roleService;
        $this->permssionsService = $permissionsService;
    }

    public function index(Request $request)
    {
        $roles = $this->roleService->searchDataByKey($request->search_key);
        $permissions = $this->permssionsService->getAll();
        return view($this->pathView."index", compact('roles', 'permissions'));
    }

    public function store(RoleRequest $request)
    {
        $this->roleService->saveRoleData($request->all());
        return $this->sendResponse("Thêm mới thành công");
    }

    public function edit($id)
    {
        $data = $this->roleService->getDatasByID($id);
        return response()->json([
            'status_code' => 200,
            'role' => $data['role'],
            'permissions' => $data['permissions'] 
        ]);
    }
   
    public function update(RoleRequest $request)
    {
        $this->roleService->updateByID($request->all());
        return $this->sendResponse("Cập nhật thành công");
    }

    public function delete($id)
    {
        $this->roleService->deleteByID($id);
        return $this->sendResponse();
    }
}
