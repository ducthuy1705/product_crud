<?php

namespace App\Http\Controllers;

use App\Http\Requests\PermissionRequest;
use App\Models\Permissions;
use App\Services\PermissionsService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionsController extends Controller
{
    protected $pathView = 'admin.permissions.';
    protected $permissionsService;

    public function __construct(PermissionsService $permissionsService)
    {
        $this->permissionsService = $permissionsService;
    }

    public function index(Request $request)
    {   
        $permissions = $this->permissionsService->searchDataByKey($request->search_key);
        return view($this->pathView.'index', compact('permissions'));
    }

    public function store(PermissionRequest $request)
    {
        $this->permissionsService->saveData($request->all());
        return $this->sendResponse("Thêm mới thành công");
    }

    public function update(PermissionRequest $request)
    {
        $this->permissionsService->updateByID($request->all());
        return $this->sendResponse("Cập nhật thành công");
    }

    public function delete($id)
    {
        $this->permissionsService->deleteByID($id);
        return $this->sendResponse();
    }
}
