<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'phone' => 'numeric',
            'password' => 'required|confirmed',
            'roles' => 'required',
            'image' => 'mimes:png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được rỗng',
            'email' => ':attribute phải dưới dạng email @',
            'numeric' => ':attribute phải là số',
            'unique' => ':attribute đã tồn tại',
            'min:8' => ':attribute phải 8 kí tự',
            'confirmed' => ':attribute không khớp'
        ];
    }
}
