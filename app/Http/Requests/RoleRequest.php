<?php

namespace App\Http\Requests;

use App\Rules\Required;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'permissions' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được rỗng',
            'unique' => ':attribute đã tồn tại',
        ];
    }
}
