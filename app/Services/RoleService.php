<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll()
    {
        return $this->roleRepository->getAll();
    }

    public function searchDataByKey($key)
    {
        return $this->roleRepository->searchData($key);
    }

    public function paginateData()
    {
        return $this->roleRepository->paginate();
    }

    public function saveRoleData($data)
    {
        return $this->roleRepository->save($data);
    }

    public function getDatasByID($id)
    {
        return $this->roleRepository->getRoleWithPermissions($id);
    }

    public function updateByID($data)
    {
        return $this->roleRepository->update($data);
    }

    public function deleteByID($id)
    {
        return $this->roleRepository->deleteWithRelation($id);
    }
}