<?php

namespace App\Services;

use App\Repositories\ProductCategoriesRepository;

class ProductCategoriesService
{
    protected $productCategoriesRepository;

    public function __construct(ProductCategoriesRepository $productCategoriesRepository)
    {
        $this->productCategoriesRepository = $productCategoriesRepository;
    }

    public function paginateData()
    {
        return $this->productCategoriesRepository->paginate();
    }

    public function searchDataByKey($key)
    {
        return $this->productCategoriesRepository->searchData($key);
    }

    public function getAll()
    {
        return $this->productCategoriesRepository->getAll();
    }

    public function saveData($data)
    {
        return $this->productCategoriesRepository->save($data);
    }

    public function updateByID($data)
    {
        return $this->productCategoriesRepository->update($data);
    }

    public function deleteByID($id)
    {
        return $this->productCategoriesRepository->delete($id);
    }
}
