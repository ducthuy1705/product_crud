<?php

namespace App\Services;

use App\Repositories\PermissionsRepository;

class PermissionsService
{
    protected $permissionsRepository;

    public function __construct(PermissionsRepository $permissionsRepository)
    {
        $this->permissionsRepository = $permissionsRepository;
    }

    public function paginateData()
    {
        return $this->permissionsRepository->paginate();
    }

    public function searchDataByKey($key)
    {
        return $this->permissionsRepository->searchData($key);
    }

    public function getAll()
    {
        return $this->permissionsRepository->getAll();
    }

    public function saveData($data)
    {
        return $this->permissionsRepository->save($data);
    }

    public function updateByID($data)
    {
        return $this->permissionsRepository->update($data);
    }

    public function deleteByID($id)
    {
        return $this->permissionsRepository->delete($id);
    }
}
