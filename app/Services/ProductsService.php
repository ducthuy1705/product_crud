<?php

namespace App\Services;

use App\Repositories\ProductsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductsService
{
    protected $productsRepository;

    public function __construct(ProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    public function paginateData()
    {
        return $this->productsRepository->paginate();
    }

    public function getDataByID($id)
    {
        return $this->productsRepository->getSingleRecord($id);
    }

    public function searchDataByKey(Request $request)
    {
        $option = $request->search_option;
        $key = $request->search_key;
        return $this->productsRepository->search($option, $key);
    }

    public function saveData($requests)
    {
        $data = [];
        foreach($requests as $key => $request)
        {
            $data[$key] = $request;

            if(is_object($request))
            {
                $data[$key] = $this->uploadImage($request);
            }
        }
        return $this->productsRepository->save($data);
    }

    public function updateByID($requests)
    {
        $data = [];
        foreach($requests as $key => $request)
        {
            $data[$key] = $request;

            if(is_object($request))
            {
                $data[$key] = $this->uploadImage($request);
            }
        }
        return $this->productsRepository->update($data);
    }

    public function deleteByID($id)
    {
        return $this->productsRepository->delete($id);
    }

    public function uploadImage($object)
    {
        $fileType = $object->getClientOriginalExtension();
        $randomFileName = Str::random(12).'.'.$fileType;
        $path = public_path().'/picture/product/';
        $object->move($path, $randomFileName);

        return $randomFileName;
    }
}
