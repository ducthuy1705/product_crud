<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function paginateData()
    {
        return $this->userRepository->paginate();
    }

    public function saveData(Request $request)
    {
        $data = [];
        $input = $this->getInput($request);

        foreach($input as $key => $value)
        {
            $data[$key] = $value;

            if($key == "password")
            {
                $data[$key] = bcrypt($value);
            }
            
            if(is_object($value))
            {
                $data[$key] = $this->uploadImage($value);
            }
        }

        return $this->userRepository->saveUserAndAttachRole($data, $request->roles);
    }


    public function getUserByID($id)
    {
        return $this->userRepository->getData($id);
    }

    public function updateUser(Request $request)
    {
        if($request->password == null)
        {
            $input = $request->only(['id', 'name', 'phone', 'email', 'avatar']);
        }
        else
        {
            $input = $this->getInput($request);
        }

        $data = [];

        foreach($input as $key => $value)
        {
            $data[$key] = $value;

            if($key == "password")
            {
                $data[$key] = bcrypt($value);
            }

            if(is_object($value))
            {
                $data[$key] = $this->uploadImage($value);
            }
        }

        return $this->userRepository->updateUserAndSyncRole($data, $request->roles);
    }

    public function deleteByID($id)
    {
        return $this->userRepository->deleteWithRelation($id);
    }

    public function searchDataByKey(Request $request)
    {
        $option = $request->search_option;
        $value = $request->search_key;
        return $this->userRepository->searchUsers($option, $value);
    }

    public function getInput($request)
    {
        return $request->only(['id', 'name', 'phone', 'email', 'password', 'avatar']);
    }

    public function uploadImage($object)
    {
        $fileType = $object->getClientOriginalExtension();
        $randomFileName = Str::random(12).'.'.$fileType;
        $path = public_path().'/picture/users/';
        $object->move($path, $randomFileName);

        return $randomFileName;
    }
}
