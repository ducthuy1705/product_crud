<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductCategoriesPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission(['productCategories.view']);
    }

    public function create(User $user)
    {
        return $user->hasPermission(['productCategories.create']);
    }

    public function update(User $user)
    {
        return $user->hasPermission(['productCategories.update']);
    }

    public function delete(User $user)
    {
        return $user->hasPermission(['productCategories.delete']);
    }
}
