<?php

namespace App\Policies;

use App\Models\Products;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductsPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        return $user->hasPermission(['product.view']);
    }

    public function create(User $user)
    {
        return $user->hasPermission(['product.create']);
    }

    public function update(User $user)
    {
        return $user->hasPermission(['product.update']);
    }

    public function delete(User $user)
    {
        return $user->hasPermission(['product.delete']);
    }

}
