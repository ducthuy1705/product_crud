<?php

namespace App\Providers;

use App\Policies\UserPolicy;
use App\Policies\RolePolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ProductsPolicy;
use App\Policies\ProductCategoriesPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->productPolicies();
        $this->rolePolicies();
        $this->userPolicies();
        $this->permissionPolicies();
        $this->productCategoriesPolicies();
    }

    public function productPolicies()
    {
        Gate::define("product.view", [ProductsPolicy::class, "view"]);
        Gate::define("product.create", [ProductsPolicy::class, "create"]);
        Gate::define("product.update", [ProductsPolicy::class, "update"]);
        Gate::define("product.delete", [ProductsPolicy::class, "delete"]);
    }

    public function rolePolicies()
    {
        Gate::define("role.view", [RolePolicy::class, "view"]);
        Gate::define("role.create", [RolePolicy::class, "create"]);
        Gate::define("role.update", [RolePolicy::class, "update"]);
        Gate::define("role.delete", [RolePolicy::class, "delete"]);
    }

    public function userPolicies()
    {
        Gate::define("user.view", [UserPolicy::class, "view"]);
        Gate::define("user.create", [UserPolicy::class, "create"]);
        Gate::define("user.update", [UserPolicy::class, "update"]);
        Gate::define("user.delete", [UserPolicy::class, "delete"]);
    }

    public function permissionPolicies()
    {
        Gate::define("permission.view", [PermissionPolicy::class, "view"]);
        Gate::define("permission.create", [PermissionPolicy::class, "create"]);
        Gate::define("permission.update", [PermissionPolicy::class, "update"]);
        Gate::define("permission.delete", [PermissionPolicy::class, "delete"]);
    }

    public function productCategoriesPolicies()
    {
        Gate::define("productCategories.view", [ProductCategoriesPolicy::class, "view"]);
        Gate::define("productCategories.create", [ProductCategoriesPolicy::class, "create"]);
        Gate::define("productCategories.update", [ProductCategoriesPolicy::class, "update"]);
        Gate::define("productCategories.delete", [ProductCategoriesPolicy::class, "delete"]);
    }
}
