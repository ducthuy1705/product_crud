<?php

namespace App\Repositories;

use App\Models\Permissions;

class PermissionsRepository extends BaseRepository
{
    public function model()
    {
        return Permissions::class;
    }
}
