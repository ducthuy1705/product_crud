<?php

namespace App\Repositories;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

abstract class BaseRepository
{
    protected $model;

    abstract public function model();

    public function makeModel()
    {
        $this->model = app($this->model());
    }

    public function __construct()
    {
        $this->makeModel();
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getSingleRecord($id)
    {
        return $this->model->findOrFail($id);
    }

    public function paginate()
    {
        return $this->model->paginate(10);
    }

    public function save($data)
    {
        return $this->model->create($data);
    }

    public function update($request)
    {
        $data = $this->getSingleRecord($request['id']);
        return $data->update($request);
    }

    public function delete($id)
    {
        $data = $this->getSingleRecord($id);
        $data->delete();
        return $data;
    }

    public function searchData($key)
    {
        $data = $this->model->searchMultiple($key)
            ->orderBy('id')
            ->paginate(10)
            ->appends(request()->query());
        return $data;
    }
}
