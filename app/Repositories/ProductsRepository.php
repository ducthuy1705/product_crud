<?php

namespace App\Repositories;

use App\Models\Products;

class ProductsRepository extends BaseRepository
{
    public function model()
    {
        return Products::class;
    }

    public function search($option, $key)
    {
        return $this->model->whereHasCategory($option)
            ->whereHasKey($key)
            ->orderBy('id')
            ->paginate(10)
            ->appends(request()->query());
    }
}
