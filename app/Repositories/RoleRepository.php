<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    
    public function model()
    {
        return Role::class;
    }

    public function save($data)
    {
        $role = $this->model->create([
            'name' => $data['name'],
            'status' => $data['status'],
        ]);

        $permissions = $this->getPermissionsID($data['permissions']);
        $role->attachPermissions($permissions);

        return $role;
    }

    public function getRoleWithPermissions($id)
    {
        $data['role'] = $this->getSingleRecord($id);
        $data['permissions'] = $data['role']->permissions->pluck('id');
        return $data;
    }

    public function update($data)
    {
        $role = $this->getSingleRecord($data['id']);
        $permissions = $this->getPermissionsID($data['permissions']);
        
        $role->update([
            'name' => $data['name'],
            'status' => $data['status'],
        ]);
        
        $role->syncPermissions($permissions);
        return $role;
    }

    public function deleteWithRelation($id)
    {
        $role = $this->getSingleRecord($id);
        $role->detachPermissions();
        $role->delete();
        return $role;
    }

    public function getPermissionsID($permissions)
    {
        $getPermissions = [];
        foreach($permissions as $permission)
        {
            $getPermissions[] = $permission;
        }
        return $getPermissions;
    }
}
