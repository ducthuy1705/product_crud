<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use App\Repositories\BaseRepository;
use PhpParser\Node\Expr\FuncCall;

class UserRepository extends BaseRepository
{
    public function model()
    {   
        return User::class;
    }

    public function getData($id)
    {
        $data['user'] = $this->getSingleRecord($id);
        $data['roles'] = $data['user']->roles->pluck('id');

        return $data;
    }

    public function saveUserAndAttachRole($data, $roles)
    {   
        $user = $this->model->create($data);
        $roleID = $this->getRoleID($roles);
        $user->attachRole($roleID);

        return $user;
    }

    public function updateUserAndSyncRole($data, $roles)
    {   
        $user = $this->getSingleRecord($data['id']);
        $user->update($data);
        $roleID = $this->getRoleID($roles);
        $user->syncRole($roleID);

        return $user;
    }

    public function getRoleID($roles)
    {
        $roleID = [];
        foreach($roles as $role)
        {
            $roleID[] = $role;
        }
        return $roleID;
    }

    public function deleteWithRelation($id)
    {
        $user = $this->getSingleRecord($id);
        $user->detachRole();
        $user->delete();
        return $user;
    }

    public function searchUsers($option, $key)
    {
        $data = $this->model->whereRoleHas($option)
            ->whereUserHas($key)
            ->orderBy('id')
            ->paginate(10)
            ->appends(request()->query());
        return $data;
    }
}
