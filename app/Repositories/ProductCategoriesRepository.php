<?php

namespace App\Repositories;

use App\Models\ProductCategories;

class ProductCategoriesRepository extends BaseRepository
{
    public function model()
    {
        return ProductCategories::class;
    }
}
