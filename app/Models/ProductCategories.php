<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'image', 'status', 'slug'];

    public function products()
    {
        return $this->hasMany("App\Models\Products", "category_id");
    }

    public function scopeSearchMultiple($query, $key)
    {
        return $key ? $query->where('name', 'like', "%{$key}%")
            ->orWhere('slug', 'like', "%{$key}%") : null;
    }
}
