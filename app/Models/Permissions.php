<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'action'];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_permissions', 'id_permission', 'id_role');
    }

    public function scopeSearchMultiple($query, $key)
    {
        return $key ? $query->where('name', 'like', "%{$key}%")
            ->orWhere('action', 'like', "%{$key}%") : null;
    }
}
