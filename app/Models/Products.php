<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'description', 'image', 'price_base', 'price_total',
        'category_id', 'quantity', 'status', 'slug'
    ];

    public function category()
    {
        return $this->belongsTo("App\Models\ProductCategories", "category_id");
    }

    public function getImage()
    {
        return asset("picture/product/".$this->image);
    }

    public function scopeWhereHasCategory($query, $category)
    {
        return $category ? $query->whereHas('category', function(Builder $query) use($category){
            $query->where('name', $category);
        }) : null;
    }

    public function scopeWhereHasKey($query, $key)
    {
        return $key ? $query->where('name', 'like', "%{$key}%")
            ->orWhere('description', 'like', "%{$key}%") : null;
    }
}
