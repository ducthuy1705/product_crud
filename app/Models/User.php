<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['name', 'email', 'password', 'phone', 'avatar'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'user_roles', 'id_user', 'id_role');
    }

    public function hasRole($roles){
        foreach($roles as $role)
        {
            if($this->roles->contains('name', $role))
            {
                return true;
            }
        }
        return false;
    }

    public function hasPermission($perrmissions)
    {
        foreach($this->roles as $role)
        {
            if($role->hasPermission($perrmissions)){
                return true;
            }
        }
        return false;
    }

    public function attachRole($roles)
    {
        return $this->roles()->attach($roles);
    }

    public function syncRole($roles)
    {
        return $this->roles()->sync($roles);
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }

    public function getImage()
    {
        return asset('/picture/users/'.$this->avatar);
    }

    public function scopeWhereRoleHas($query, $role)
    {
        return $role ? $query->whereHas('roles', function(Builder $query) use($role) {
            $query->where('name', $role);
        }) : null;
    }

    public function scopeWhereUserHas($query, $key)
    {
        return $key ? $query->where('name', 'like', "%{$key}%")
            ->orWhere('email', 'like', "%{$key}%")
            ->orWhere('phone', 'like', "%{$key}%") : null;
    }
}
