<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'status'];
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'user_roles', 'id_role', 'id_user');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permissions', 'role_permissions', 'id_role', 'id_permission');
    }

    public function hasPermission($permissions)
    {
        foreach($permissions as $permission)
        {
            if($this->permissions->contains('action', $permission)){
                return true;
            }
        }
        return false;
    }

    public function attachPermissions($permission)
    {
        return $this->permissions()->attach($permission);
    }

    public function syncPermissions($permission)
    {
        return $this->permissions()->sync($permission);
    }

    public function detachPermissions()
    {
        return $this->permissions()->detach();
    }

    public function scopeSearchMultiple($query, $key)
    {
        return $key ? $query->where('name', 'like', "%{$key}%") : null;
    }
}
