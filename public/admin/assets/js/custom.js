/*============================Menu Table===================
- 1, Helper Functions
- 2, crud users
- 3, crud permissions
- 4, crud roles
- 5, crud product categories
- 6, crud product
*/

(function($, window, document) {

    $(function() {
    /* ============================= 1, Helper Functions  ============================= */
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $("meta[name='crsf-token']").attr("content")
            },
        });    

        $(".multiple-select-create").select2({
            dropdownParent: $('#create-user'),
            tags: true,
            tokenSeparators: [',', ' ']
        });

        $(".multiple-select-update").select2({
            dropdownParent: $('#update-user'),
            tags: true,
            tokenSeparators: [',', ' ']
        });

        $("#select-category").select2({
            dropdownParent: $('#create-product'),
            tags: true,
            tokenSeparators: [',', ' '],
            minimumResultsForSearch: Infinity
        });

        $("#select-category-update").select2({
            dropdownParent: $('#update-product'),
            tags: true,
            tokenSeparators: [',', ' '],
            minimumResultsForSearch: Infinity
        });

        $("#select-role").select2({
            minimumResultsForSearch: Infinity
        });

        $(".checkAllBox").on('click', function() {
            $(this).parents('.check-permission').find(".check-all").prop("checked", $(this).prop("checked"));
        })

        $(".clear-search").on('click', function(e) {
            e.preventDefault();
            var pathName = window.location.pathname;
            window.location.href = pathName;
        })

        var inputName = $('.input-name');
        inputName.keydown(function(){
            $(this).parent(".form-group").find('#nameError').addClass('d-none');
        })

        inputName.on("change",function(){
            var value = $(this).val();
            slug = ChangeToSlug(value);
            $('.input-slug').val(slug);
        });

        function ChangeToSlug(select_nguon) {
            var title, str;
            // Chuyển hết sang chữ thường
            str = select_nguon;
            str = str.toLowerCase();
            // Đổi ký tự có dấu thành không dấu
            str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            str = str.replace(/(đ)/g, 'd');
            // Xóa ký tự đặc biệt
            str = str.replace(/([^0-9a-z-\s])/g, '');
            // Xóa khoảng trắng thay bằng ký tự -
            str = str.replace(/(\s+)/g, '-');
            // xóa phần dự - ở đầu
            str = str.replace(/^-+/g, '');
            // xóa phần dư - ở cuối
            str = str.replace(/-+$/g, '');
            // return
            return str;
        }

        function getSuccessAlert(data)
        {
            var alert = $('.sweet-alert .alert-success');    
            alert.find('#message').text(data);
            alert.fadeIn('slow');
            setInterval(function (){
                location.reload().fadeIn("slow");
            }, 1000)
        }

        function getErrorsForCreate(errors)
        {
            if($.isEmptyObject(errors) == false)
            {
                $.each(errors.errors, function(key, value){
                    var errorID = "#" + key + 'Error';
                    $(errorID).removeClass('d-none').text(value);
                })
            }
        }

        function getErrorsForUpdate(errors)
        {
            if($.isEmptyObject(errors) == false)
            {
                $.each(errors.errors, function(key, value){
                    var errorID = "#" + key + 'UpdateError';
                    $(errorID).removeClass('d-none').text(value);
                })
            }
        }

        function postAjax(url, data)
        {
            return $.ajax({
                type: 'POST',
                url: url,
                data: data,
                processData: false,
                cache: false,
                contentType: false,
            })
        }

        function getAjax(url)
        {
            return $.ajax({
                type: 'GET',
                url: url,
            })
        }

        $('.delete-button').on('click', function(e){
            e.preventDefault();
            var _this = $(this); 
            var url = $(this).data('url');
            Swal.fire({
                title: 'Thông Báo',
                text: "Bạn có chắc muốn xóa role này không ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'DELETE',
                        url: url,

                        success: function (data){
                            Swal.fire(
                                'Xóa thành công',
                                'Dữ liệu đã được xóa.',
                                'thành công'
                            )

                            if(data.status_code === 200){
                                _this.closest("tr").remove();
                            }
                        },
                    })
                }
            })
        })
    /* ============================= 2, Crud User  ============================= */
        $('#formCreateUser').on('submit', function(e){
            e.preventDefault();
            var url = $(this).data('url');
            var formData = new FormData(this);

            var createUser = postAjax(url, formData);
            createUser.done(function(data) {
                if(data.status_code === 200)
                {
                    $("#create-user").modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForCreate(errors);
            })
        })
    
        $('.update-button').on('click', function(e)
        {
            e.preventDefault();
            const url = $(this).data('url');
            
            var getUser = getAjax(url);
            getUser.done(function(data){
                if(data.status_code == 200)
                {
                    $("#formUpdateUser").find('#id-update').val(data.user.id);
                    $("#formUpdateUser").find('#name-update').val(data.user.name);
                    $("#formUpdateUser").find('#phone-update').val(data.user.phone);
                    $("#formUpdateUser").find('#email-update').val(data.user.email);
                    if(data.user.avatar != null)
                    {
                        $("#formUpdateUser").find('.file-upload img')
                            .attr('src', 'http://localhost:9000/picture/users/'+data.user.avatar);
                    }
                    //Cho id roles của user vào array và set selected cho select2 với array đó
                    const valueOption = data.roles.map((value) => {
                        return value;
                    })

                    $("#formUpdateUser").find('#roles-update').val(valueOption).change();

                    $("#update-user").on('hidden.bs.modal', function () {
                        $(this).find('#formUpdateUser').trigger('reset');
                    })
                }
            })
        })

        $("#formUpdateUser").on('submit', function(e){
            e.preventDefault();
            const url = $(this).data('url');
            const formData = new FormData(this);

            var updateUser = postAjax(url, formData)

            updateUser.done(function(data) {
                if(data.status_code === 200)
                {
                    $("#update-user").modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForUpdate(errors);
            })
        })


    /* ============================= 3, Crud Permissions  ============================= */
        $('#formCreatePermission').on('submit', function(e){
            e.preventDefault();
            
            var url = $(this).data('url');
            var formData = new FormData(this);
            
            var createPermission = postAjax(url, formData);
            
            createPermission.done(function(data) {
                if(data.status_code === 200)
                {
                    $("#create-permission").modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForCreate(errors);
            })
        });

        $('.update-buttuon').on('click', function(e){
            e.preventDefault();

            const data = $(this).closest('tr').children('td').map(function(){
                return $(this).text();
            }).get();

            $('#id-permission').val(data[0]);
            $('#name-permission').val(data[1]);
            $('#action-permission').val(data[2]);
        });

        $('#formUpdatePermission').on('submit', function(e){
            e.preventDefault();
            const url = $(this).data('url');
            const formData = new FormData(this);

            var updatePermission = postAjax(url, formData);

            updatePermission.done(function(data) {
                if(data.status_code === 200)
                {
                    $('.update-button').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForUpdate(errors);
            })
        })

    /* ============================= 4, Crud roles  ============================= */
        $('#formCreateRole').on('submit', function(e){
            e.preventDefault();
            
            var url = $(this).data('url');
            var data = new FormData(this);

            var createRole = postAjax(url, data);

            createRole.done(function(data) {
                if(data.status_code === 200)
                {
                    $("#create-role").modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForCreate(errors);
            })
        })

        $('.get-role-button').on('click', function(e)
        {
            e.preventDefault();
            var url = $(this).data('url');

            var getRole = getAjax(url);

            getRole.done(function(data){
                if(data.status_code == 200)
                {
                    $("#formUpdateRole").find('#id-role').val(data.role.id);
                    $("#formUpdateRole").find('#name-role').val(data.role.name);

                    if(data.role.status == "active")
                    {
                        $("#formUpdateRole").find('#active-role').prop("checked", true);
                    }

                    //Cho id roles của user vào array và set selected cho select2 với array đó
                    var checkbox = $("#formUpdateRole").find('.check-all');

                    const valuePermissions = data.permissions.map((value) => {
                        return value;
                    })

                    checkbox.each(function() {
                        //Convert value to number to compare value of array
                        if(valuePermissions.includes( parseInt($(this).val()) ))
                        {
                            $(this).prop("checked", true);
                        }
                    })
                    
                    //Reset data of modal after close it
                    $('#update-role').on('hidden.bs.modal', function () {
                        $(this).find('#formUpdateRole').trigger('reset');
                    })
                }
            })
        })
    
        $('#formUpdateRole').on('submit', function(e) {
            e.preventDefault();

            var url = $(this).data('url');
            var formData = new FormData(this);

            var updateRole = postAjax(url, formData);

            updateRole.done(function(data) {
                if(data.status_code == 200)
                {
                    $('.get-role-button').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForUpdate(errors);
            })
        })

        /* ============================= 5, Crud Produc Categories  ============================= */
        $("#formCreateProductCategories").on("submit", function(e) {
            e.preventDefault();

            var url = $(this).data('url');
            var formData = new FormData(this);
            var createProductCategories = postAjax(url, formData);

            createProductCategories.done(function(data) {
                if(data.status_code === 200)
                {
                    $('.update-productC-button').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForCreate(errors);
            })
        })

        $('.update-productC-button').on('click', function(e){
            e.preventDefault();

            const data = $(this).closest('tr').children('td').map(function(){
                return $(this).text();
            }).get();

            $('#id-productCategories').val(data[0]);
            $('#name-productCategories').val(data[1]);
            $('#slug-productCategories').val(data[2]);
            if(data[3] === "active")
            {
                $("#active-productCategories").prop("checked",true);
            }

            $('#update-productCategories').on('hidden.bs.modal', function () {
                $(this).find('#formUpdateProductCategories').trigger('reset');
            })
        });
        
        $("#formUpdateProductCategories").on('submit', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            var formData = new FormData(this);

            var updateProducCategory = postAjax(url, formData);

            updateProducCategory.done(function(data) {
                if(data.status_code === 200)
                {
                    $('.update-productC-button').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForUpdate(errors);
            });
        })

        /* ============================= 6, Crud Product  ============================= */
        $("#formCreateProduct").on('submit', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            var formData = new FormData(this);

            var createProduct = postAjax(url, formData);

            createProduct.done(function(data) {
                if(data.status_code === 200)
                {
                    $('#create-product').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForCreate(errors);
            })
        })

        $(".update-product-button").on('click', function(e) {
            e.preventDefault();
            var url = $(this).data('url');
            var updateProduct = getAjax(url);

            updateProduct.done(function (data) {
                if(data.status_code === 200)
                {
                    var form = $("#formUpdateProduct");
                    form.find('#id_product').val(data.product.id);
                    form.find('#name-product').val(data.product.name);
                    form.find('#name-slug').val(data.product.slug);
                    form.find('#description-product').val(data.product.description);
                    form.find('#price_base-product').val(data.product.price_base);
                    form.find('#price_final-product').val(data.product.price_final);
                    form.find('#quantity-product').val(data.product.quantity);
                    form.find('#select-category-update').val(data.product.category_id).change();

                    if(data.product.status === "active")
                    {
                        form.find('#status-product').prop("checked",true);
                    }

                    $('#update-product').on('hidden.bs.modal', function () {
                        $(this).find('#formUpdateProduct').trigger('reset');
                    })
                }
            })
        })

        $("#formUpdateProduct").on('submit', function(e) {
            e.preventDefault();

            var url = $(this).data('url');
            var formData = new FormData(this);

            var updateProduct = postAjax(url, formData);

            updateProduct.done(function(data) {
                if(data.status_code === 200)
                {
                    $('#update-product').modal('hide');
                    getSuccessAlert(data.message);
                }
            }).fail(function(data) {
                var errors = data.responseJSON;
                getErrorsForUpdate(errors);
            })
        })
    })

}(window.jQuery, window, document));
